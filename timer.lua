Timer = {
  methods = {},
  tweens = {}
}

function Timer:after(t, f)
   table.insert(Timer.methods, {fin = t, func = f, accum = 0, exec = 1})
end

function Timer:every(t, f)
  table.insert(Timer.methods, {fin = t, func = f, accum = 0})
end

function Timer:tween(iObj, interval)
  local f, key, final
  for ltable, rtable in pairs(iObj) do
    f = ltable
    for ikey, ivalue in pairs(rtable) do
      key = ikey
      final = rtable[ikey]
    end
  end
  table.insert(Timer.tweens, {lTable = f, key = key, initial = f[key], final = final, interval = interval, accum = 0})
end

function Timer:updateTweener(dt)
  for i=1, #Timer.tweens, 1 do
    local current = Timer.tweens[i]
    local ltable, key, initial, final = current.lTable, current.key, current.initial, current.final
    local t = current.accum / current.interval
    current.accum = current.accum + dt
    if t < 1 then
      ltable[key] = initial * (1 - t) + final * t
    else
      ltable[key] = final
      table.remove(Timer.tweens, i)
    end
  end
end

function Timer:updateMethods(dt)
  for i=1, #Timer.methods, 1 do
    local current = Timer.methods[i]
    if current.accum >= current.fin then
      current.func()
      if current.exec ~= nil then
        current.exec = current.exec - 1
        if current.exec == 0 then
          table.remove(Timer.methods, i)
        end
      else
        current.accum = 0
      end
    else
      current.accum = current.accum + dt
    end
  end
end

function Timer:update(dt)
  Timer:updateMethods(dt)
  Timer:updateTweener(dt)
end
